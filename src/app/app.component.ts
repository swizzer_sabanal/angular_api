import { Component } from '@angular/core';

import { UsersService } from './users.service';
import { tap } from 'rxjs';
import { FormGroup, FormControl, FormControlName } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ExamAngular';
  myUsersData$: any;
  myUsersData: any;

  constructor(private user:UsersService) {
  }

  ngOnInit(): void {
    this.myUsersData$ = this.user
      .getUsers()
      .pipe(tap((data => (this.myUsersData = data))));
  }

  userForm = new FormGroup({
    username: new FormControl(''),
    firstname: new FormControl(''),
    lastname: new FormControl('')
  });

  addUser() {
    console.warn(this.userForm.value);
  }
}
