import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }

  getUsers() {
    let url = 'https://localhost:7054/api/Users/Get'
    return this.http.get(url);
  }
}
